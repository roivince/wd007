<?php
	echo 'welcome home';

	//use semi colons
	echo "<br>" . "hello";
	echo 'world ';

	// $username = "Jane Smith";
	// echo $username;
	// echo "<br>";
	// $current_user = $username;
	// echo $current_user:

	$tuitt = array('kato-san', 'shem-san', 'ali', 'angeli', 'carmela' );

	echo "<br>" . $tuitt[2]; //this should aoutput ali in the broswer

//	associative arrays are arrays that store key => value pairs
	//syntax
	//associative = array (key=>value);

	$assoc = array ('key1' => 'value');
	echo "<br>" . $assoc['key1'];
	echo "<br>";
	$assoc2 = array();
	$assoc2['key2'] = "value2";
	$assoc2['key3'] = "value3";
	//dumps information about the specified variable
	var_dump($assoc2);
	//a cleaner, more readable and usable version of var_dump is var_export
	echo "<br>";
	var_export($assoc2);

	//constant syntax
	//define('name' , 'value');

	define('MY_GREETING', 'hoy unggoy');
	//this will output an error since you cannot redefine a constant
	//define('MY_GREETING', 'hello');
	$greet_me = MY_GREETING;
	echo "<br>";
	echo $greet_me;

	//assignment expression
	//we use the = to assign a value to a variable
	$x = 10;
	$y = 25;

	$x += 10;
	// $x = $x + 10;
	echo "<br>" . $x;

	$y %= $x;
	//$y = $y %x;
	echo '<br>' . $y;








?>