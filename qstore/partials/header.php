<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, shrink-to-fit=no"> 
  	<meta http-equiv="X-UA-Compatible" content="IE-Edge">

	<title> Qstore | <?php echo getTitle(); ?> </title>

	<!-- fav icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">

	<!-- font awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	
	<!-- animate css -->
	<link rel="stylesheet" href="../assets/css/style.css">

	<!-- google fonts -->

	<!-- bootstrap css and js dependencies -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- external css -->
  	<link rel="stylesheet" href="assets/css/style.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<script src="../assets/js/script.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-success"> <!-- navbar-expand-lg (burger) -->
		<a href="#" class="navbar-brand animated bounce flip">
			<i class="fas fa-charging-station"></i> R+
		</a>

		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-nav">
			<span class="navbar-toggler-icon"></span> <!-- (burger icon) -->
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a href="../views/register.php" class="nav-link">
					<i class="fas fa-home"></i> Register </a>
				</li>

				<li class="nav-item">
					<a href="../views/login.php" class="nav-link">
					<i class="fas fa-globe"></i> Login </a>
				</li>

				<!-- <li class="nav-item">
					<a href="#" class="nav-link">
					<i class="fas fa-satellite-dish"></i> Contacts </a>
				</li>

				<li class="nav-item">
					<a href="#" class="nav-link">
					<i class="fas fa-rocket"></i> Gallery </a>
				</li> -->
			</ul>
		</div>
	</nav> <!-- end nav -->

	
	
</body>