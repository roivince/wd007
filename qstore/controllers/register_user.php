<?php 
	// PHP has predefined variables which are designed to collect data sent by HTML forms. These superglobal variables are $_POST and $_GET.
	// superglobal variables simply means that it is a specially-defined variable(normally arrays) that can be accessed anywhere in our program

	// the request returned an array where in it established the name attribute of the input as the key and the value inputted to it as its value

	// var_dump($_POST);
	var_dump($_POST["firstName"]);
	// var_dump($_GET);
	// var_dump($_GET["confirmPassword"]);

	// GET and POST does the same thing as both variables handle html form data but the main difference is when you use GET, the data we entered to the form will be displayed in the URL. POST, on the other hand, sends the form data behind the scenes. Thus, not seeing the form data in the url.

	//sanitize our form inputs
	$fname = htmlspecialchars($_POST['firstName']);
	$lname = htmlspecialchars($_POST['lastName']);
	$email = htmlspecialchars($_POST['email']);

	//lets encrypt our password to make it more secure
	$password = sha1(htmlspecialchars($_POST['password']));
	$confirmPassword = sha1(htmlspecialchars($_POST['confirmPassword']));
	var_dump($password);
	var_dump($confirmPassword);

	// form a new assoc array of the sanitized inputs
	$newUser = ["firstName" => $fname, "lastName" => $lname, "email" => $email, "password" => $password];

	// check how the new assoc array looks
	var_dump($newUser);

	// the isset() function checks if a variable or an element had been assigned a value
	// in this case, the isset function is checking if the keys firstName and lastName in the POST request has been assigned a value

	if(isset($_POST['firstName']) && isset($_POST['lastName'])) {
		echo "Welcome " . $_POST['firstName']. " " . $_POST['lastName'] . "<br>";
	} else {
		echo "Please provide a complete name";
	}


	if (isset($_POST['email'])) {
		echo 'Your email is '.$_POST['email']."<br>";
	}	else {
		echo 'Please provide an email';
	}


	//we are going to be storing to a JSON file. JSON stands for Javascript Object Notation. It is used in exchanging and storing data from the web server. JSON uses the object notation of Javascript.

	// check if password and confirmPassword are both set and not null
	if ($_POST['password'] != "" && $_POST['confirmPassword'] !="") {
		if($password===$confirmPassword) {
			//retrieve the contents of accounts.json and convert to a string
			//	file_get_contents(filepath that you want to get)
			$json = file_get_contents('../assets/lib/accounts.json');
			// var_dump($json);
			// convert the string to associative array. when true, the json_decode function converts the json string to an assoc array
			//syntax: json_decode(jsonfile, option)

			$accounts = json_decode($json, true);
			// var_dump($accounts);

			// push the contents of the form data from the $_POST request to the end of the $accounts assoc array
			// syntax: array_push(array, value to be pushed)
			array_push($accounts, $newUser);

			// var_dump($accounts);

			// fopen function opens the file for writing
			// syntax: fopen(file to be opened, mode of access)
			// w opens the file for writing only
			$to_write = fopen('../assets/lib/accounts.json', 'w');

			// fwrite function writes to opened file(filename)
			// syntax: fwrite(file to write in, string to be written)
			// json_encode function converts the php array to a json string
			// syntax: json_encode(value, options)
			// var_dump(json_encode($accounts));
			// JSON_PRETTY_PRINT option add white spaces that makes json strings more is_readable(filename)
			// fwrite($to_write, json_encode($accounts));
			fwrite($to_write, json_encode($accounts, JSON_PRETTY_PRINT));

			//close the previously opened file to free up resources
			fclose($to_write);

			
			
			// redirect to the login page
			header('Location: ../views/login.php');

			echo'<br> passwords match';
		} else {
			echo 'passwords did not match';
		}
	} else {
		echo "Please fill out the password fields";
	}




 ?>