<?php
	// single line comment
	/*
		multiple line comment
	*/

	// to serve a project, we use the syntax
		// php -S address:port
		// php -S localhost:8000
		// php -S 127.0.0.1:8000

	// to make our web server accessible by remote machines, serve it using the 0.0.0.0 address
		// php -S 0.0.0.0:8000

	// php stands for php:hypertext preprocessor
	// it allows devs to take what used to be a static html content and make it responsive to the client's user's request, or do the same permanently-stored data that resides in a database

	//Variable Naming Rules
		//a variable is declared using a $ followed by the variable name
		//variables must start with a letter of the alphabet or an underscore
		//variable names can contain a-z, A-Z, 0-9 and _(underscore)
		//variable names must not contain spaces. 
		//if a variable must comprise more than one word, we separate the words using them _(eg. $user_name)
		//variables are case-sensitive. the variable $HIGH_SCORE is not the same as variable $high_score
	$name = 'roi';
	$age = 18;

	//same as print $name;
	echo $name;

	//single quotes will treat variables as regular strings
	echo '<h1>$age</h1>';
	//double quotes will output the value of the variable
	echo "<h1>$age</h1>";

	//we concatenate using the . symbol
	echo "<h1>My name is ". $name. " I am " .$age. " years old </h1>";

	//END a statement with a semi colon

	//the header location will redirect to the indicated path,
	//in this case, ./views/home.php
	header('Location: ./views/home.php');


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<h1>My age is : <?php echo $age; ?> </h1>
	
</body>
</html>