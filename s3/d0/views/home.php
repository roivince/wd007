<?php
	// echo 'hello';

	//functions are used when you need to run a block of code multiple times. instead of writing multiple-liner codes multiple times, we can call back a function that will execute the same thing instead.

	//basic syntax
	// function functionName(){
	// 	code block to be run
	// }

	// function sayHello(){
	// 	echo "Hello";
	// }

	// creating a function does not execute it. You need to call it in order for it to run

	//function with parameters
	//parameters can serve as placeholders/inputs so the functions can behave differently with each function call

	// function sayHello($name){
	// 	echo "hello" . $name;
	// }
	// sayHello("roi");
	// sayHello("tuitt");
	
	// we can make functions have return values. return values are that are being returned to the caller once the function end
	
	// function addNum ($num1, $num2) {
	// 	$sum = $num1 + $num2;
	// 	return $sum;
	// }
	// echo addNum(3,5);
	// echo addNum(5,5);

// print out a 2x3 table

	// function print2x3(){
	// 	echo "<table border='1'>";
	// 	for($i=0; $i<2; $i++) {
	// 		echo "<tr>";
	// 			for($j=0; $j<3; $j++) {
	// 				echo "<td> Content </td>";
	// 			}
	// 		echo "</tr>";
	// 	}
	// 	echo "</table>";
	// }

	// print2x3();


	// function print2x3 ($rows, $cols) {
	// 	echo "<table border='1'>";
	// 		for ($i=0; $i<$rows; $i++) {
	// 			echo "<tr>";
	// 				for ($j=0; $j<$cols; $j++) {
	// 					echo "<td> Content </td>";
	// 				}
	// 			echo "</tr>";
	// 		}
	// 	echo "</table>";
	// }
	// print2x3(1,1);
	// print2x3(2,3);
	// print2x3(3,5);

	
	// print the day today
	// function longdate ($timestamp){
		//Y - numeric representation of the year (2019, 1999, etc)
		// l full textual representation of the day of the month
		// F full textual representation of the month
		// j day without the leading 0s
		// S - english suffix the th, nd , etc
		// Y - numeric representation of the year (2019, 1999, etc)


	// 	return date("l F jS Y z ", $timestamp);
	// }
	// the time() function is a built-in PHP funct ion that return the time eplased since unix Epoch (January 1, 1970 00:00:00 thursday). The date function will then convert it to a more readable format by following the format we specified

	// echo time();
	// echo longdate(time());

	// commonly used built in PHP functions
	// strtolower() - converts a string to lowercase
	// strtoupper() - converts a string to uppercase
	// ucfirst() - capitalizes the first letter of the string
	// strlen() - returns the length of the string
	// trim() - removes trailing white spaces
	// ltrim() - removes whitespaces before the string
	// htmlspecialchars() - converts predefined characters to HTML entities to prevent the browser from treating predefined html elements as elemets and render them as string

	// $lowered = strtolower("ANY # oF LETTers and PUNCTUation yOu want <br>");
	// echo $lowered;

	// $capital = ucfirst("any #of letters and punctuation you want <br>");
	// echo $capital;


	function fix_names($first, $middle, $last) {
		// $first = ucfirst("roiVincE"); 
		$first = strtolower($first); 
		$middle = strtolower($middle);
		$last = strtolower($last);
			$first2 = ucfirst($first); 
		$middle2 = ucfirst($middle);
		$last2 = ucfirst($last);
		echo $first2." ".$middle2." ". $last2;
		

	}

	echo fix_names ("roiVincE", "siSoN", "TaN");

	
	// function fix_names($first, $middle, $last) {
	// 	$first = strtolower($first); 
	// 	$middle = strtolower($middle);
	// 	$last = strtolower($last);
	// 	return $first . " ".$middle. " ". $last;

	// }
	// echo fix_names ("roiVincE", "siSoN", "TaN");





	// function sayHello($name){
	// 	echo "hello" . $name;
	// }
	// sayHello("roi");
	// sayHello("tuitt");



?>