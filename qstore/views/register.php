<?php
	// the require statement is the same as the include statement in the sense that it allows you to reference files from different locations. the only difference with include and require is the error handling. The include statement will throw a warning but still run the code while require will throw a fatal error. include_once and require_once will do the same thing but once the file is already included/required already, it will not include/require it again.
	
	// include '../partials/header.php';
	// require '../partials/header.php';
	require_once '../partials/header.php';
	// include_once '../partials/header.php';

echo 'hello';

	function getTitle(){
		return "Register Page";
	}
?>

	<div class="container-fluid">
		<h2 class="text-center">Registration Page</h2>
		<div class="row">
			<div class="col-md-8 mx-auto">
				<!-- the action attribute sets the destination to which the form data will be submitted. the value of the action attribute can be relative or an absolute url -->
				<!-- the method attribute specifies the type of HTTP request you want to make upon submission of this form POST and GET -->
				<form action="../controllers/register_user.php" method="POST">

					<div class="form-group">
						<label for="firstName">First Name</label>
						<input type="text" id="firstName" name="firstName" class="form-control">
					</div>

						<div class="form-group">
						<label for="lastName">Last Name</label>
						<input type="text" id="lastName" name="lastName" class="form-control">
					</div>

					<div class="form-group">
						<label for="email">Email</label>
						<input type="text" id="email" name="email" class="form-control">
					</div>

					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" id="password" name="password" class="form-control">
					</div>
					
					<div class="form-group">
						<label for="password">Confirm Password</label>
						<input type="password" id="confirmPassword" name="confirmPassword" class="form-control">
					</div>

					<!-- the type submit/button will submit the form -->
				<button id="sub" type="submit" class="btn btn-primary float-right custom-btn custom-btn-primary">Submit</button>

				</form>
			</div>
		</div>
	</div>	
	<!-- end container -->





<?php

	require '../partials/footer.php';

?>

